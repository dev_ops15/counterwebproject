package com.qaagility.controller;

public class Count2 {

    public int getValue(int number1, int number2) {
        if (number2 == 0){
            return Integer.MAX_VALUE;
        }
	else{
            return number1 / number2;
    	}
    }
}
