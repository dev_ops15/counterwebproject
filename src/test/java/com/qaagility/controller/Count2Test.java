package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class Count2Test {
    
	@Test
        public void testCountIf() throws Exception {
                int result = new Count2().getValue(10, 0);
                assertEquals("getValue : if statement", Integer.MAX_VALUE, result);
        }

	@Test
        public void testCountIf2() throws Exception {
                int result = new Count2().getValue(142, 0);
                assertEquals("getValue : if statement", 2147483647, result);
        }

	@Test
        public void testCountElse() throws Exception {
                int result = new Count2().getValue(12, 6);
                assertEquals("getValue : else statement", 2, result);
        }

}
